<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Jelly &#124; Celebrating Bermuda's Creatives</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>

<div id="page">
	<?php include('inc/header.php'); ?>
	<div id="content">

		<div class="search-bar">
			<div class="container">
				<form action="" method="POST">
					<input type="text" class="search">
					<div class="search-hint bg-purple2">
						<span>Suggested Search:</span>
						<ul>
							<li><a href="#">music</a>,</li>
							<li><a href="#">live</a>,</li>
							<li><a href="#">graphic artists</a>,</li>
							<li><a href="#">photographers</a></li>
						</ul>
					</div>
				</form>
			</div>
		</div>

		<section class="page-section">
			<div class="container">

				<h2 class="text-center">where local creativity thrives</h2>

				<h3>Latest posts</h3>
				<div class="row">
					<div class="block-3">
						<div class="grid-image"><a href="post.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<span class="post-meta">February 15, 2017</span>
						<h4>First post title</h4>
						<p>Bacon ipsum dolor amet tri-tip meatball pork chop shankle beef ribs short ribs pork. Tongue shank drumstick leberkas cupim rump.</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="post.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<span class="post-meta">February 15, 2017</span>
						<h4>Title of the second post</h4>
						<p>Jowl flank jerky ball tip capicola, pig rump filet mignon cupim. Turducken bacon burgdoggen meatball alcatra.</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="post.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<span class="post-meta">February 15, 2017</span>
						<h4>The third post always has a super long title and tries to break your layout</h4>
						<p>Shoulder shankle kielbasa t-bone rump. Spare ribs venison porchetta prosciutto bacon leberkas biltong ham hock.</p>
					</div>
				</div>

				<div class="btn-wrap">
					<a href="blog.php" class="btn">More posts</a>
				</div>

			</div>
		</section>

		<section class="page-section">
			<div class="container">

				<h3>Latest profiles</h3>
				<div class="row">
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>Gregory Beercraft</h4>
						<p>Designer</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>John Swankleton</h4>
						<p>Photographer</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>Jane Doe</h4>
						<p>DJ/Event Manager</p>
					</div>
				</div>

				<div class="btn-wrap">
					<a href="discover.php" class="btn">More profiles</a>
				</div>

			</div>
		</section>

	</div><!-- #content -->
	<?php include('inc/footer.php'); ?>
</div><!-- #page -->

<script src="js/plugins.min.js"></script>
<script src="js/main.min.js"></script>
</body>
</html>
