<?php

	$pagetitle = "Jelly &#124; Celebrating Bermuda's Creatives";

?>
<header class="site-header bg-purple">
	<div class="container">
		<a href="index.php" class="logo">
			<!-- The h1 is hidden, it's good to set a dynamic page title here -->
			<h1><?php echo $pagetitle; ?></h1>
			<img src="img/jelly-logo.png" alt="Jelly logo" width="600" height="245" />
		</a>
		<nav class="site-nav">
			<ul>
				<li>
					<a href="index.php">
						<img src="img/icon-home.png" width="100" height="100" />
						<span>Home</span>
					</a>
				</li>
				<li>
					<a href="blog.php">
						<img src="img/icon-blog.png" width="100" height="100" />
						<span>Blog</span>
					</a>
				</li>
				<li>
					<a href="discover.php">
						<img src="img/icon-search.png" width="100" height="100" />
						<span>Discover</span>
					</a>
				</li>
			</ul>
		</nav>
	</div>
</header>
