<footer class="site-footer bg-blue">
	<div class="container">
		<div class="site-meta">
			<h5>About Jelly!</h5>
			<p>Bacon ipsum dolor amet tri-tip meatball pork chop shankle beef ribs short ribs pork. Tongue shank drumstick leberkas cupim rump.</p>
		</div>
		<ul class="social-links">
			<li><a href="#"><img src="img/icon-fb.png" alt="facebook" width="100" height="100" /></a></li>
			<li><a href="#"><img src="img/icon-instagram.png" alt="instagram" width="100" height="100" /></a></li>
		</ul>
	</div>
</footer>
