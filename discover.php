<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Jelly &#124; Celebrating Bermuda's Creatives</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>

<div id="page">
	<?php include('inc/header.php'); ?>
	<div id="content">

		<section class="page-section">
			<div class="container">

				<h2 class="text-center">discover&hellip;</h2>

				<div class="row">
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>Gregory Beercraft</h4>
						<p>Designer</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>John Swankleton</h4>
						<p>Photographer</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>Jane Doe</h4>
						<p>DJ/Event Manager</p>
					</div>
				</div>

				<div class="row">
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>Jane Doe</h4>
						<p>DJ/Event Manager</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>Gregory Beercraft</h4>
						<p>Designer</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>John Swankleton</h4>
						<p>Photographer</p>
					</div>
				</div>

				<div class="row">
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>Gregory Beercraft</h4>
						<p>Designer</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>John Swankleton</h4>
						<p>Photographer</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>Jane Doe</h4>
						<p>DJ/Event Manager</p>
					</div>
				</div>

				<div class="row">
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>Jane Doe</h4>
						<p>DJ/Event Manager</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>Gregory Beercraft</h4>
						<p>Designer</p>
					</div>
					<div class="block-3">
						<div class="grid-image"><a href="profile.php"><img src="img/image_thumb_640x360.jpg" width="640" height="360" alt="Insert_alt_description" /></a></div>
						<h4>John Swankleton</h4>
						<p>Photographer</p>
					</div>
				</div>

			</div>
		</section>

	</div><!-- #content -->
	<?php include('inc/footer.php'); ?>
</div><!-- #page -->

<script src="js/plugins.min.js"></script>
<script src="js/main.min.js"></script>
</body>
</html>
