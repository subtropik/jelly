<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Jelly &#124; Celebrating Bermuda's Creatives</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>

<div id="page">
	<?php include('inc/header.php'); ?>
	<div id="content">

		<section class="page-section post">
			<div class="container">

				<div class="feature-image"><img src="img/image_feature_1920x1080.jpg" width="1920" height="1080" alt="Insert_alt_description" /></div>

				<h2>Post title</h2>
				<span class="post-meta">February 15, 2017</span>

				<div class="post-content">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec viverra porta sapien non maximus. Cras placerat, tellus et tristique ultrices, felis est hendrerit leo, id consequat ante dolor id dui. Proin bibendum tellus ut justo sodales feugiat. Proin posuere vitae nunc eu facilisis. Praesent elit eros, pellentesque eget semper et, feugiat non sapien. Praesent commodo nisi enim, nec bibendum elit porta quis. Nunc malesuada purus sed est vestibulum hendrerit. Morbi gravida rutrum purus. Suspendisse egestas dapibus arcu, volutpat efficitur lacus. Integer quis mollis dui, volutpat ultrices enim. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean placerat dapibus odio id dictum. Mauris vitae erat quam. Duis vitae euismod lorem, congue venenatis augue. Suspendisse ullamcorper orci quis arcu imperdiet, porttitor fringilla ex pharetra. Fusce varius vel ligula ut condimentum.</p>

					<p>Mauris nec lacus quis risus fermentum venenatis ut vel sapien. Donec fringilla arcu ut justo accumsan, at malesuada orci congue. Nulla maximus neque ipsum, in aliquet leo fermentum et. Morbi auctor, tellus a cursus semper, quam leo lacinia sapien, vitae sagittis lorem leo vel eros. Cras aliquet ultricies turpis quis laoreet. Nullam lacinia ultrices diam ac vestibulum. Nulla euismod enim enim, et commodo mi pharetra a. Ut a posuere nunc, at blandit ipsum. Integer vel leo risus. Vivamus sit amet egestas arcu, non hendrerit est.</p>

					<p>Quisque consectetur nunc nunc, nec blandit purus congue quis. Vivamus ultricies, dui ac gravida vestibulum, nunc libero cursus purus, at maximus metus ligula ac lorem. Morbi lacus sem, euismod sed mi ac, molestie mattis odio. Vestibulum maximus pellentesque egestas. Pellentesque tortor nibh, euismod sed ornare sit amet, fermentum ac sem. Ut erat augue, rhoncus ut turpis nec, pellentesque hendrerit justo. Quisque ex odio, pharetra ut facilisis sit amet, maximus vitae purus. Nam vitae massa ipsum. Nullam pellentesque, sapien non venenatis elementum, sapien ex luctus nisl, eget finibus velit nisl quis turpis. Maecenas vel ipsum rhoncus, rutrum ipsum finibus, vehicula turpis.</p>
				</div>

			</div>
		</section>

	</div><!-- #content -->
	<?php include('inc/footer.php'); ?>
</div><!-- #page -->

<script src="js/plugins.min.js"></script>
<script src="js/main.min.js"></script>
</body>
</html>
